#include "OutStream.h"
#include <stdio.h>

namespace msl
{


	OutStream::OutStream()
	{
	}

	OutStream::~OutStream()
	{
	}

	OutStream& OutStream::operator<<(const char *str)
	{
		fprintf(stdout, "%s", str);
		return *this;
	}

	OutStream& OutStream::operator<<(int num)
	{
		fprintf(stdout, "%d", num);
		return *this;
	}

	OutStream& OutStream::operator<<(void(*pf)())
	{
		pf();
		return *this;
	}


	void OutStream::endline()
	{
		fprintf(stdout, "\n");
	}
}