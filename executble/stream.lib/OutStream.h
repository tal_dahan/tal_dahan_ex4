#pragma once
#include <stdio.h>

namespace msl
{

	class OutStream
	{
	protected:
		FILE *f;
	public:
		OutStream();
		~OutStream();

		OutStream& operator<<(const char *str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)());
		void endline();
	};

}