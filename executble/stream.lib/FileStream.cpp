#include "FileStream.h"
namespace msl
{

	FileStream::FileStream(const char*  file)
	{
		fopen_s(&f, file, "w");
	}
	FileStream::~FileStream()
	{
		fclose(f);
	}

	FileStream& FileStream::operator<<(const char *str)
	{
		fprintf(f, "%s", str);
		return *this;
	}
	FileStream& FileStream::operator<<(int num)
	{
		fprintf(f, "%d", num);
		return *this;
	}
	FileStream& FileStream::operator<<(void(*pf)())
	{
		pf();
		return *this;
	}
	void FileStream::endline()
	{
		fprintf(f, "\n");
	}
}