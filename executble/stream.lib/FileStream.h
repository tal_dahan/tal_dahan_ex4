#pragma once
#include "OutStream.h"
#include<string>


namespace msl
{
	class FileStream :public OutStream
	{
	public:
		FileStream(const char*  file);	//constructor
		~FileStream();//distructor
		FileStream& operator<<(const char *str);//write strings in file
		FileStream& operator<<(int num);//write numbers in file
		FileStream& operator<<(void(*pf)());
		void endline();
	};
}
