#include <stdio.h>
#pragma comment (lib,"../Debug/stream.lib.lib")
#include "../stream.lib/FileStream.h"
#include "../stream.lib/OutStream.h"

using namespace msl;
int main(int argc, char **argv)
{
	//section 1
	OutStream o = OutStream();

	o << "I am the Doctor and I�m ";
	o << 1500;
	o << " years old";
	//section 2
	FileStream f = FileStream("t.txt");
	f << "I am the Doctor and I'm ";
	f << 1500;
	f << " years old";
	
	getchar();
	return 0;
}