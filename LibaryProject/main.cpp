#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"
#include <stdio.h>
using namespace std;
int main(int argc, char **argv)
{
	//section 1
	OutStream o = OutStream();
	
	o << "I am the Doctor and I�m ";
	o << 1500;
	o << " years old";
	//section 2
	FileStream f = FileStream("t.txt");
	f << "I am the Doctor and I'm ";
	f << 1500;
	f << " years old";
	//section 3
	OutStreamEncrypted d = OutStreamEncrypted();
	d.endline();
	d << "I am the Doctor and I�m ";
	d << 1500;
	d << " years old";
	d.endline();
	//section 4+5
	Logger l = Logger(); 
	Logger l2 = Logger();
	l<< "I am the Doctor and I�m ";
	l2 << 1500;
	l << " years old";
	getchar();
	return 0;
}
