#include "OutStreamEncrypted.h"
#include "OutStream.h"


using namespace std;

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char *str)
{
	
	unsigned int len = 0;
	int i = 0; 
	OutStream o = OutStream();
	this->key = 3;
	
	//finde the len of the string
	len = strlen(str);
	//creat arr len of the given string
	char *text = new char[len+1];
	//creat result string 
	char *result = new char[len + 1];
	//insert the characters to the array
	for (i; i < len+1; i++)
	{
		text[i] = str[i];		
	}
	text[i] = 0;
	
	
	// traverse text 
	for ( i = 0; i < len+1; i++)
	{
		// apply transformation to each character 
		result[i]= char(int(text[i] + this->key - 32) % 95 + 32);
		// print the resulting string
		
	}
	result[i] = 0;
	/*print the result*/
	o << result;
	return *this;
}

OutStreamEncrypted&OutStreamEncrypted:: operator<<(int num)
{
	OutStreamEncrypted o = OutStreamEncrypted();
	//convers the num to char* and send it to the Encryption function
	string temp_str = to_string(num);
	char const* pchar = temp_str.c_str();
	o << pchar;
	return *this;
}

OutStreamEncrypted&OutStreamEncrypted:: operator<<(void(*pf)())
{
	pf();
	return *this;
}