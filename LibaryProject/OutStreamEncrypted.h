#include "OutStream.h"
#include "FileStream.h"


class OutStreamEncrypted:public OutStream
{
private:
	unsigned int key;//key for the "jump" in caesar cipher
public:
	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)());
};