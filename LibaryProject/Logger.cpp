#include "Logger.h"

#include <stdio.h>

using namespace std;
/*creat static varible*/
static unsigned int lines = 0;

Logger::Logger()
{
	this->_startLine = true;
	this->os = OutStream();
}
Logger& operator<<(Logger& l, const char *msg)
{
	/*check if startLine is true - we need to print the messege*/
	if (l._startLine)
	{
		lines++;
		l.setStartLine();
		fprintf(stdout, "%s", "LOG ");
		fprintf(stdout, "%d", lines);
		fprintf(stdout, "%s", "- ");
		l.os << msg;

		l.os.endline();
		l.setStartLine();

	}
	return l;
}
Logger& operator<<(Logger& l, int num)
{
	/*check if startLine is true - we need to print the messege*/
	if (l._startLine)
	{
		lines++;

		l.setStartLine();
		fprintf(stdout, "%s", "LOG ");
		fprintf(stdout, "%d", lines);
		fprintf(stdout, "%s", "- ");
		l.os << num;

		l.os.endline();
		l.setStartLine();
	}
	return l;
}
Logger::~Logger()
{
	/*we dont need to do something becuse we never alloc memory in this class*/
}

Logger& operator<<(Logger& l, void(*pf)())
{
	pf();
	return l;
}

void Logger::setStartLine()
{
	this->_startLine = !this->_startLine;
}


